// global.js
$(document).ready(function(){
    bsCustomFileInput.init();
    $('.deleteFile').on('click', function(){
        let fileId = $(this).data('fileid');
        $.get({
            url : route.deleteFile.slice(0,-1) + fileId,
            success: function(data){
                data = JSON.parse(data);
                if(data.error === 'Success'){
                    $('#file'+fileId).remove();
                    $('#successDelete').fadeIn(1000).delay(4000).fadeOut(1000);
                }else {
                    $('#errorDelete .errorInfo').text(data.error);
                    $('#errorDelete').fadeIn(1000).delay(4000).fadeOut(1000);
                }
            }
        })
    });

    const fileInput = document.querySelector('#files_files');
    fileInput.addEventListener('change', (event) => {
        let files = event.target.files;
        let filesData = {};
        for (let file of files) {
            filesData[file.name] = file.lastModified;
        }
        console.log(JSON.stringify(filesData));
        $('#files_hiddenData').val(JSON.stringify(filesData));
    });
});