<?php

namespace App\Controller;

use App\Entity\Files;
use App\Form\FilesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(FilesType::class, new Files());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $files = $form['files']->getData();
            $lastModifDate = json_decode($form['hiddenData']->getData());
            if ($files) {
                foreach ($files as $file){
                    $fileBdd = new Files();
                    $fileName = $file->getClientOriginalName();
                    $lastEdit = new \DateTime();
                    if($lastModifDate->$fileName && (int)$lastModifDate->$fileName){
                        $lastEdit->setTimestamp((int)$lastModifDate->$fileName/1000);
                    }else {
                        $lastEdit->setTimestamp(filemtime($file));
                    }
                    $fileSize = $file->getSize()/1024;
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $fileName);
                    //@Todo Filter By Extensions
                    $finalFilename = $safeFilename.'_'.uniqid().'.'.$file->guessExtension();
                    $file->move(
                        $this->getParameter('filesPath'),
                        $finalFilename
                    );
                    $fileBdd->setFileName($finalFilename);
                    $fileBdd->setOriginalName($fileName);
                    $fileBdd->setSize($fileSize);
                    $fileBdd->setLasteditdate($lastEdit);
                    $fileBdd->setUploaddate(new \DateTime());
                    $em->persist($fileBdd);
                    $em->flush();
                }
            }
        }

        $filesUploaded = $em->getRepository(Files::class)->findBy(array(),array('lasteditdate'=>'desc'));
        return $this->render('index.html.twig', [
            'form' => $form->createView(),
            'filesUploaded' => $filesUploaded,
            'onlinePath' => $this->getParameter('onlinePath')
        ]);
    }

    public function deleteFile($fileId){
        $em = $this->getDoctrine()->getManager();
        $fileBdd = $em->getRepository(Files::class)->findOneById($fileId);
        if(!empty($fileBdd)){
            $filePath = $this->getParameter('filesPath') . $fileBdd->getFileName();
            if(file_exists($filePath)){
                if(unlink($filePath)){
                    $em->remove($fileBdd);
                    $em->flush();
                    $return = 'Success';
                }else {
                    $return = 'Failed to delete file';
                }
            }else{
                $return = 'File not exist';
            }
        }else {
            $return = 'No file exist with this id';
        }
        return new Response(json_encode(array(
            'error'=> $return
        )));
    }
}