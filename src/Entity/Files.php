<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Files
 *
 * @ORM\Table(name="files")
 * @ORM\Entity
 */
class Files
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="originalName", type="string", length=255, nullable=false)
     */
    private $originalName;

    /**
     * @var string
     *
     * @ORM\Column(name="fileName", type="string", length=255, nullable=false)
     */
    private $fileName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uploadDate", type="datetime", nullable=false)
     */
    private $uploaddate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastEditDate", type="datetime", nullable=false)
     */
    private $lasteditdate;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    /**
     * @param string $originalName
     */
    public function setOriginalName(string $originalName): void
    {
        $this->originalName = $originalName;
    }
    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return \DateTime
     */
    public function getUploaddate(): \DateTime
    {
        return $this->uploaddate;
    }

    /**
     * @param \DateTime $uploaddate
     */
    public function setUploaddate(\DateTime $uploaddate): void
    {
        $this->uploaddate = $uploaddate;
    }

    /**
     * @return \DateTime
     */
    public function getLasteditdate(): \DateTime
    {
        return $this->lasteditdate;
    }

    /**
     * @param \DateTime $lasteditdate
     */
    public function setLasteditdate(\DateTime $lasteditdate): void
    {
        $this->lasteditdate = $lasteditdate;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }


}
