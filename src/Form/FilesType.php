<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class FilesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hiddenData',HiddenType::class,[
                'mapped' => false
                ])
            ->add('files', FileType::class, [
                'label' => ' ',
                'mapped' => false,
                'required' => false,
                'multiple' => true
            ])
            ->add('Envoyer', SubmitType::class)
        ;
    }
}